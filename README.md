Féléves beadandó feladat

A féléves beandanó feladatomban két objektum lesz látható a szoba két sarkában. A mozgás a w-a-s-d gombokkal
lehetséges majd. A space és a c gombokkal a vertikálisan fel és le mozgás valósul meg.

A kamera állását és irányát az egérrel is lehet irányítani.

A fényerő változását a + és - gombokkal változtathatóra szeretném állítani.

Ha elég közel megyünk a fentebb leírt módom az objektumhoz, akkor egy gomb lenyomásával megfoghatjuk és a mozgást
elősegítő gombok segítségével elmozgatható lesz, mozgatás során a szoba különböző részén interaktívan az objektum 
színe megváltozik. 

F1 gomb lenyomásával a súgó nyílik meg, mely leírja a program működését, kezelését.